package com.andredezzy.course.java.reflection.libs.data.controller;

import com.andredezzy.course.java.reflection.libs.data.dao.ProductDao;
import com.andredezzy.course.java.reflection.libs.data.model.Product;

import java.util.List;
import java.util.stream.Collectors;

public class ProductController {

    private ProductDao productDao;

    public ProductController() {
        productDao = new ProductDao();
    }

    public List<Product> getAll() {
        return productDao.getList();
    }

    public List<Product> get(String name, String brand) {
        return getAll().stream().filter(product -> product.getName().toLowerCase().startsWith(name.toLowerCase()) && product
                .getBrand().toLowerCase().startsWith(brand.toLowerCase())).collect(Collectors.toList());
    }

    public List<Product> getByName(String name) {
        return getAll().stream().filter(product -> product.getName().toLowerCase().startsWith(name.toLowerCase()))
                .collect(Collectors.toList());
    }

    public List<Product> getByBrand(String brand) {
        return getAll().stream().filter(product -> product.getBrand().toLowerCase().startsWith(brand.toLowerCase()))
                .collect(Collectors.toList());
    }
}
