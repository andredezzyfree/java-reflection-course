package com.andredezzy.course.java.reflection.libs.protocol.manager;

import com.andredezzy.course.java.reflection.Main;
import com.andredezzy.course.java.reflection.libs.protocol.MethodBuilder;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ObjectManager {

    private Object object;

    public ObjectManager(Class<?> clazz) {
        try {
            this.object = clazz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Object getObject() {
        return object;
    }

    public MethodList getMethods(boolean declaredMethods) {
        MethodList methodList = new MethodList();

        List<MethodBuilder> tempBuilders = Arrays.stream(object.getClass().getMethods()).filter(method -> declaredMethods ||
                method.getDeclaringClass() == getObject().getClass())
                .map(MethodBuilder::new).collect(Collectors.toList());

        methodList.addAll(tempBuilders);
        return methodList;
    }

    public Object invokeMethod(Method method) {
        try {
            return method.invoke(getObject());
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            Main.error("Ocorreu um erro: " + e.getTargetException());
        } catch (IllegalArgumentException | IllegalAccessException e) {
            Main.error(String.format("Parâmetros incorretos. Formato: %s", new MethodBuilder(method).formatParameters
                    ()));
        } catch (Exception ignored) {
        }
        return null;
    }

    public Object invokeMethod(Method method, Object... args) {
        try {
            return method.invoke(getObject(), args);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            Main.error("Ocorreu um erro: " + e.getTargetException());
        } catch (IllegalArgumentException | IllegalAccessException e) {
            Main.error(String.format("Parâmetros incorretos. Formato: %s", new MethodBuilder(method).formatParameters
                    ()));
        } catch (Exception ignored) {
        }
        return null;
    }

    public Object newConstructor() {
        try {
            return getObject().getClass().getConstructor().newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static class MethodList extends ArrayList<MethodBuilder> {

        public MethodBuilder getByName(String name) {
            return super.stream().filter(method -> method.getMethod().getName().toLowerCase().startsWith(name.toLowerCase()))
                    .findFirst().orElse(null);
        }
    }
}
