package com.andredezzy.course.java.reflection.libs.data.dao;

import java.util.List;

public interface AbstractDao<T> {

    List<T> getList();
}
