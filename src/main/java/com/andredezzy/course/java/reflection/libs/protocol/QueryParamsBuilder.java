package com.andredezzy.course.java.reflection.libs.protocol;

import com.andredezzy.course.java.reflection.Main;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class QueryParamsBuilder {

    private Map<String, Object> queryParams;

    public QueryParamsBuilder(String query) {
        queryParams = new HashMap<>();
        String[] params = query.split("&");
        for (String param : params) {
            String[] keyValue = param.split("=");
            if (keyValue.length > 1) {
                String name = keyValue[0];
                String value = keyValue[1];
                queryParams.put(name, value);
            }
        }
    }

    public List<Object> valuesToList() {
        return build().entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
    }

    public Map<String, Object> build() {
        return this.queryParams;
    }
}
