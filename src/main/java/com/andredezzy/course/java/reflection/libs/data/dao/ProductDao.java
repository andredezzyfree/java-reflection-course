package com.andredezzy.course.java.reflection.libs.data.dao;

import com.andredezzy.course.java.reflection.libs.data.model.Product;

import java.util.Arrays;
import java.util.List;

public class ProductDao implements AbstractDao {

    private static final List<Product> productList =
            Arrays.asList(new Product("Product 1", 20.0, "Marca 1"),
                    new Product("Product 2", 30.0, "Marca 1"),
                    new Product("Product 3", 40.0, "Marca 2"));

    public List<Product> getList() {
        return productList;
    }
}
