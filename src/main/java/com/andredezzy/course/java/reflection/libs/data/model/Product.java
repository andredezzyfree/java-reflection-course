package com.andredezzy.course.java.reflection.libs.data.model;

public class Product {

    private String name;
    private double price;
    private String brand;

    public Product(String name, double price, String brand) {
        this.name = name;
        this.price = price;
        this.brand = brand;
    }

    @Override
    public String toString() {
        return String.format("Product{name='%s', price=%s, brand='%s'}", name, price, brand);
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getBrand() {
        return brand;
    }
}
