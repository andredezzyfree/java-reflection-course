package com.andredezzy.course.java.reflection.libs.protocol;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MethodBuilder {

    private Method method;

    public MethodBuilder(Method method) {
        this.method = method;
    }

    public Method getMethod() {
        return method;
    }

    public String formatParameters() {
        List<String> stringMethod = Arrays.stream(method.getParameters()).map(mapParameter -> String.format("%s %s", mapParameter.getType().getSimpleName(), mapParameter.getName())).collect(Collectors.toList());
        return String.join(", ", stringMethod);
    }

    public String fullyFormat() {
        return String.format("%s(%s)", method.getName(), formatParameters());
    }
}
