package com.andredezzy.course.java.reflection.libs;

import com.andredezzy.course.java.reflection.Main;
import com.andredezzy.course.java.reflection.libs.protocol.MethodBuilder;
import com.andredezzy.course.java.reflection.libs.protocol.QueryParamsBuilder;
import com.andredezzy.course.java.reflection.libs.protocol.URLRequest;
import com.andredezzy.course.java.reflection.libs.protocol.manager.ClassManager;
import com.andredezzy.course.java.reflection.libs.protocol.manager.ObjectManager;

import java.lang.reflect.Method;
import java.util.List;

public class Reflector {

    private static final String CONTROLLER_PACKAGE_NAME = "com.andredezzy.course.java.reflection.libs.data.controller";

    public Object execute(String url) {
        URLRequest urlRequest = new URLRequest(url);
        QueryParamsBuilder queryParams = null;
        if (urlRequest.getMethodParams() != null) queryParams = new QueryParamsBuilder(urlRequest.getMethodParams());

        ClassManager classManager = new ClassManager(CONTROLLER_PACKAGE_NAME);
        Class<?> finderClass = classManager.getByName(urlRequest.getClassName());

        if (finderClass != null) {
            ObjectManager objectManager = new ObjectManager(finderClass);
            Object objectInstance = objectManager.getObject();

            ObjectManager.MethodList notDeclaredMethods = objectManager.getMethods(false);
            notDeclaredMethods.stream().map(MethodBuilder::fullyFormat).forEach(System.out::println);

            if (urlRequest.getMethodName() != null) {
                MethodBuilder methodBuilder = objectManager.getMethods(true).getByName(urlRequest.getMethodName());

                if (methodBuilder != null) {
                    Method method = methodBuilder.getMethod();
                    method.setAccessible(true);

                    if (queryParams != null) {
                        List<Object> parameterList = queryParams.valuesToList();
                        return objectManager.invokeMethod(method, parameterList.toArray());

                    } else return objectManager.invokeMethod(method);
                } else Main.debug("null method!");
            } else Main.debug("null class method!");

            return objectManager.newConstructor();
        } else Main.debug("class not found!");
        return null;
    }


}
