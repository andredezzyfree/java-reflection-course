package com.andredezzy.course.java.reflection;

import com.andredezzy.course.java.reflection.libs.Reflector;

import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class Main {

    public static void debug(Object object, Object... objects) {
        if (objects.length > 0 && Arrays.stream(objects).allMatch(Objects::isNull))
            return;
        System.out.println("[DEBUG] -> " + String.format(object.toString(), Arrays.toString(objects)));
    }

    public static void error(Object object, Object... objects) {
        if (objects.length > 0 && Arrays.stream(objects).allMatch(Objects::isNull))
            return;
        System.out.println("[ERROR] -> " + String.format(object.toString(), Arrays.toString(objects)));
    }

    public static void main(String[] args) {
        System.out.println("Executing 'Java Reflection' test application:");

        /*
         * Casos possiveis:
         * /controlador/metodo
         * /controlador/metodo?param1=valor1&param2=valor2
         */

        System.out.println("Example: /produto/lista");
        System.out.println("         /controlador/metodo");

        try (Scanner s = new Scanner(System.in)) {
            String url = s.nextLine();

            Reflector reflector = new Reflector();
            while (!url.equalsIgnoreCase("exit")) {
                System.out.println("");
                Object response = reflector.execute(url);

                System.out.println("");
                System.out.printf("Response: %s%n", response);
                url = s.nextLine();
            }
        }
    }
}
