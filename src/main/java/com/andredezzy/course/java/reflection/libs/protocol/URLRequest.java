package com.andredezzy.course.java.reflection.libs.protocol;

public class URLRequest {

    private String urlQuery;
    private String className;
    private String methodName;
    private String methodParams;

    public URLRequest(String urlQuery) {
        this.urlQuery = urlQuery;
        String[] splitedUrl = urlQuery.startsWith("/") ? urlQuery.replaceFirst("/", "").split("/") : urlQuery.split("/");

        this.className = splitedUrl[0].toLowerCase();
        if (splitedUrl.length > 1) {
            this.methodName = splitedUrl[1].contains("?") ? splitedUrl[1].split("\\?")[0].toLowerCase() : splitedUrl[1]
                    .toLowerCase();
            this.methodParams = splitedUrl[1].contains("?") ? splitedUrl[1].split("\\?")[1].toLowerCase() : null;
        }
    }

    public String getClassName() {
        return className;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getMethodParams() {
        return methodParams;
    }
}
