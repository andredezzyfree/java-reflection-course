package com.andredezzy.course.java.reflection.libs.protocol.manager;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class ClassManager {

    private String controllerPackageFullyName;

    public ClassManager(String controllerPackageFullyName) {
        this.controllerPackageFullyName = controllerPackageFullyName;
    }

    public Class<?> getByName(String name) {
        List<Class> classes = getClasses(controllerPackageFullyName);
        if (classes != null)
            return classes.stream().filter(packageClass -> packageClass.getSimpleName().toLowerCase().startsWith
                    (name.toLowerCase())).findFirst().orElse(null);
        return null;
    }

    private List<Class> getClasses(String packageName) {
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            assert classLoader != null;
            String path = packageName.replace('.', '/');
            Enumeration resources = classLoader.getResources(path);
            List<File> dirs = new ArrayList();
            while (resources.hasMoreElements()) {
                URL nextElement = (URL) resources.nextElement();
                dirs.add(new File(nextElement.getFile()));
            }
            ArrayList<Class> classes = new ArrayList();
            for (File directory : dirs) classes.addAll(findClasses(directory, packageName));
            return classes;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List classes = new ArrayList();
        if (!directory.exists()) return classes;
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class"))
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
        }
        return classes;
    }

}
